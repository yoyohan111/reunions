-- pour la doc du UUID: https://phauer.com/2016/uuids-hibernate-mysql/
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `users` (
    `ID_USER` int NOT NULL AUTO_INCREMENT,
    `nom` varchar(255),
    `prenom` varchar(255) NOT NULL,
    `email` varchar(63) NOT NULL UNIQUE,
    `password` varchar(60) NOT NULL,
    `role` int NOT NULL DEFAULT 0,  --  Utilisateur = 0, Manager = 1, Admin = 2
    -- Contraintes
    PRIMARY KEY (`ID_USER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `groupe` (
    `ID_GROUPE` int NOT NULL AUTO_INCREMENT,
    `date_creation` DATETIME NOT NULL DEFAULT NOW(),
    `titre` varchar(255) NOT NULL UNIQUE,
    `photo_path` varchar(255),
    PRIMARY KEY (`ID_GROUPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `relation_user_groupe` (
    `user_id` int NOT NULL, 
    `groupe_id` int NOT NULL,
    `present` boolean NOT NULL DEFAULT FALSE,
    -- Contraintes
    PRIMARY KEY (`user_id`,`groupe_id`),
    FOREIGN KEY (`user_id`) REFERENCES users(`ID_USER`) ON DELETE CASCADE,
    FOREIGN KEY (`groupe_id`) REFERENCES groupe(`ID_GROUPE`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `compte_rendu` (
    `ID_COMPTE_RENDU` int NOT NULL AUTO_INCREMENT,
    `resume` varchar(2047) NOT NULL,
    -- Contraintes
    PRIMARY KEY (`ID_COMPTE_RENDU`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `reunion` (
    `ID_REUNION` int NOT NULL AUTO_INCREMENT,
    `MANAGER_ID` int NOT NULL,
    `GROUPE_ID` int NOT NULL,
    `COMPTE_RENDU_ID` int,
    `date_ajout` DATETIME NOT NULL DEFAULT NOW(),
    `sujet` varchar(255) NOT NULL,
    `plan` varchar(2047) NOT NULL,
    `datee` varchar(255) NOT NULL,
    `duree` int NOT NULL,
    `adresse_lieu` varchar(255) NOT NULL,
    -- Contraintes
    PRIMARY KEY (`ID_REUNION`),
    FOREIGN KEY (`MANAGER_ID`) REFERENCES users(`ID_USER`) ON DELETE CASCADE,
    FOREIGN KEY (`GROUPE_ID`) REFERENCES groupe(`ID_GROUPE`) ON DELETE CASCADE,
    FOREIGN KEY (`COMPTE_RENDU_ID`) REFERENCES compte_rendu(`ID_COMPTE_RENDU`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
