-- Suppression des tables avec des relations
DROP TABLE IF EXISTS reunion;
DROP TABLE IF EXISTS compte_rendu;
DROP TABLE IF EXISTS relation_user_groupe;
DROP TABLE IF EXISTS groupe;
DROP TABLE IF EXISTS users;