# Instructions d'installation pour la base de donnée
### Le diagramme Visual Paradigme est [ICI](diagramme_entite_relation.vpp).

**Il est recommendé d'utiliser MySql Workbench ou Wamp server**

1. Créer un nouveau Shémas appelé `reunion`
2. Pour importer les tables dans le bon ordre (pour garder les relations), exécuter le fichier `creer_tables.sql`
3. Insérer des données avec le fichier `insert_tables.sql`
4. pour Supprimer les tables dans le bon ordre, exécuter le fichier `supp_tables.sql` 