# Projet de compte-rendu de réunions

### Ce projet est fait dans le cours de C# en A2020 avec Moumene

## Pour l'installation de la base de donnée avant le démmarage

- Lire le fichier [INSTRUCTIONS_BD](BD/INSTRUCTIONS_BD.md) dans le dossier `BD` du projet.
- **Voici le diagramme de relation :**
![Screenshot](BD/shemas_pic.PNG)
## Pour l'ouverture du projet:

1. Avoir [Visual Studio](https://visualstudio.microsoft.com/downloads/) installé dans votre OS.
2. Aller dans le dossion `reunions` et double-cliquer sur le fichier .sln (la solution de Visual Studio).
3. Démarrer la solution avec Ctrl+F5 (ou juste F5 qui inclue le débuggage, mais exécute plus lentement).
## Par:

- [Yohan Gagnon-Knackstedt](@yoyohan111)
- [Mohammad Sayem](@SuperSayem)

[Le PowerPoint](https://docs.google.com/presentation/d/1F-QqkvjIrlKDuES3SrnYE_KMFNoIDzkc44EB3eDb6xc/edit?usp=sharing)
