﻿using reunions.Models;
using reunions.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace reunions.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Inscription()
        {
            if (Session["connected"] != null)
            {
                return RedirectToAction("Index", "Membre");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Inscription(UserSignup userSignup)
        {
            if (ModelState.IsValid)
            {
                bool result = UserServices.Inscrire(userSignup);
                if (result)
                {
                    TempData["message"] = "Vous êtes inscrit, veuillez maintenant vous connecter.";
                    return RedirectToAction("Login");
                }
                else
                {
                    TempData["error"] = "Inscription refusée pour " + userSignup.email;
                    return View();
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            if (Session["connected"] != null)
            {
                return RedirectToAction("Index", "Membre");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Login(User u)
        {
            if (Session["connected"] == null && ModelState.IsValid)
            {
                User user = UserServices.CheckLogin(u.email, u.password);
                if (user != null)
                {
                    Session["connected"] = user;
                    TempData["message"] = "Bienvenue " + user.nom;
                    return RedirectToAction("Index", "Membre");
                }
                else
                {
                    TempData["error"] = "Informations de connexion incorrectes.";
                    u.password = "";
                }
            }
            return View();
        }
        //public ActionResult Deconnexion()
        //{
        //    Session.Remove("connected");
        //    TempData["message"] = "Vous êtes Déconnecté";
        //    return RedirectToAction("Index", "Home");
        //}
    }
}