﻿using reunions.Models;
using reunions.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace reunions.Controllers
{
    public class MembreController : Controller
    {
        public ActionResult Deconnexion()
        {
            Session.Remove("connected");
            TempData["message"] = "Vous êtes Déconnecté";
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Plan()
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            } else
            {
                TempData["error"] = "Vous Devez choisir une réunion ici:";
                return RedirectToAction("MesReunions", "Membre");
            }
        }
        [HttpPost]
        public ActionResult Plan(int id)
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else
            {
                Reunion reu = ReunionServices.findById(id);
                if (reu != null)
                {
                    return View(reu);
                }
                else
                {
                    TempData["error"] = "ERREUR: La réunion avec un id de " + id + " n'a pas été trouvé.";
                    return View();
                }
            }
        }

        [HttpGet]
        public ActionResult MesGroupes()
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            User conn_user = (User)Session["connected"];
            if (conn_user == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Groupe> groupes = GroupeServices.findAllForUser(conn_user.id);
                return View(groupes);
            }
        }

        [HttpPost]
        public ActionResult MesGroupes(int user_id)
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Groupe> gr = GroupeServices.findAllForUser(user_id);
                //Reunion reu = ReunionServices.findById(id);
                if (gr != null)
                {
                    return View(gr);
                }
                else
                {
                    TempData["error"] = "ERREUR: L'utilisateur avec un id de " + user_id + " n'a pas été trouvé.";
                    return View();
                }
            }
        }

        [HttpGet]
        public ActionResult MesReunions()
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            User conn_user = (User)Session["connected"];
            if (conn_user == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Reunion> reunions = ReunionServices.findAllForUser(conn_user.id);
                return View(reunions);
            }
        }

        [HttpPost]
        public ActionResult MesReunions(int user_id)
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Reunion> re = ReunionServices.findAllForUser(user_id);
                //Reunion reu = ReunionServices.findById(id);
                if (re != null)
                {
                    return View(re);
                }
                else
                {
                    TempData["error"] = "ERREUR: L'utilisateur avec un id de " + user_id + " n'a pas été trouvé.";
                    return View();
                }
            }
        }

        [HttpGet]
        public ActionResult CreerGroupe()
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            User conn_user = (User)Session["connected"];
            if (conn_user == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else if (conn_user.role == 0)
            {
                TempData["error"] = "Vous devez vous connecter en temps que Gérant pour utiliser cette fonctionnalité!";
                return RedirectToAction("Login", "Home");
            }
            else    //Enfin s'il est connecté en temps que Gérant
            {
                List<User> dtusers = UserServices.GetAllUtilisateurs();
                if (dtusers.Count != 0)
                    ViewData["users"] = dtusers;
                return View();
            }
        }

        [HttpPost]
        public ActionResult CreerGroupe(Groupe groupe, HttpPostedFileBase photo_groupe) 
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            User conn_user = (User)Session["connected"];
            if (conn_user == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else if (conn_user.role == 0)
            {
                TempData["error"] = "Vous devez vous connecter en temps que Gérant pour utiliser cette fonctionnalité!";
                return RedirectToAction("Login", "Home");
            }
            else
            {
                string chkUsers = Request.Form[name: "ListeUsers"]; //ici je réussi à prendre tout les value des checkbox qui ont été cochés dans la vue!!!
                string[] arr = chkUsers.Split(',');
                List<User> users = new List<User>();
                foreach (var id_user in arr)
                {
                    User u = new User();
                    u.id = Int32.Parse(id_user);
                    users.Add(u);
                }
                groupe.membres = users;

                //Sauvegarde de la photo:
                string fichierDestination = Server.MapPath("App_Data") + "\\" + groupe.photo_path;
                try
                {
                    photo_groupe.SaveAs(fichierDestination);
                }
                //TODO: afficher messages pour utilisateur de réussite...
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Erreur, photo du groupe non sauvé: " + ex.Message);
                }

                bool result = GroupeServices.Creer(groupe);
                if (result)
                {
                    TempData["message"] = "Création du groupe effectué avec succès!";
                    List<User> dtusers = UserServices.GetAllUtilisateurs();
                    if (dtusers.Count != 0)
                        ViewData["users"] = dtusers;
                    return RedirectToAction("CreerGroupe");
                }
                else
                {
                    TempData["error"] = "Erreur dans la création du groupe [" + groupe.titre+ "]. /n Veuillez vous assurer que tout les champs sont valides.";
                    return View();
                }
            }
        }

        [HttpGet]
        public ActionResult CreerReunion()
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            User conn_user = (User)Session["connected"];
            if (conn_user == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else if (conn_user.role == 0)
            {
                TempData["error"] = "Vous devez vous connecter en temps que Gérant pour utiliser cette fonctionnalité!";
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<Groupe> groupes = GroupeServices.findAll();
                if (groupes.Count != 0)
                {
                    ViewData["dtgroupes"] = groupes;
                }
                return View();
            }
        }

        [HttpPost]
        public ActionResult CreerReunion(Reunion reunion)
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            User conn_user = (User)Session["connected"];
            if (conn_user == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else if (conn_user.role == 0)
            {
                TempData["error"] = "Vous devez vous connecter en temps que Gérant pour utiliser cette fonctionnalité!";
                return RedirectToAction("Login", "Home");
            }
            else
            {
                string id_groupe_string = Request.Form[name: "GroupeId"];
                int id_groupe = Int32.Parse(id_groupe_string);
                Groupe gr = new Groupe();
                gr.id = id_groupe;
                reunion.manager = conn_user;
                reunion.groupe = gr;

                bool result = ReunionServices.Creer(reunion);
                if (result)
                {
                    TempData["message"] = "Création de la réunion effectué avec succès!";
                    List<Groupe> groupes = GroupeServices.findAll();
                    if (groupes.Count != 0)
                    {
                        ViewData["dtgroupes"] = groupes;
                    }
                    return View();
                }
                else
                {
                    TempData["error"] = "Erreur dans la création de la réunion [" + reunion.sujet + "]. /n Veuillez vous assurer que tout les champs sont valides.";
                    return View();
                }
            }
        }

        [HttpGet]
        public ActionResult GestionUsers()
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            User conn_user = (User)Session["connected"];
            if (conn_user == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else if (conn_user.role == 0)
            {
                TempData["error"] = "Vous devez vous connecter en temps que Gérant pour utiliser cette fonctionnalité!";
                return RedirectToAction("Login", "Home");
            }
            else
            {
                List<User> users = UserServices.findAll();
                if (users.Count != 0)
                {
                    ViewData["users"] = users;
                }
                return View(users);
            }
        }
        [HttpGet]
        public ActionResult ChangeRoleUser(int id)
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            User conn_user = (User)Session["connected"];
            if (conn_user == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else if (conn_user.role == 0)
            {
                TempData["error"] = "Vous devez vous connecter en temps que Gérant pour utiliser cette fonctionnalité!";
                return RedirectToAction("Login", "Home");
            }
            else
            {
                bool result = UserServices.changerRole(id);
                if (result)
                {
                    TempData["message"] = "Changement du rôle effectué avec succès!";
                    List<User> users = UserServices.findAll();
                    if (users.Count != 0)
                    {
                        ViewData["users"] = users;
                    }
                    return RedirectToAction("GestionUsers", "Membre");
                }
                else
                {
                    TempData["error"] = "Erreur dans le changement du role du compte [" + id + "].";
                    return View();
                }
            }
        }
        [HttpGet]
        public ActionResult DeleteUser(int id)
        {
            if (Session["connected"] == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            User conn_user = (User)Session["connected"];
            if (conn_user == null)
            {
                TempData["error"] = "Vous devez vous connecter pour utiliser la plateforme!";
                return RedirectToAction("Login", "Home");
            }
            else if (conn_user.role == 0)
            {
                TempData["error"] = "Vous devez vous connecter en temps que Gérant pour utiliser cette fonctionnalité!";
                return RedirectToAction("Login", "Home");
            }
            else
            {
                bool result = UserServices.delete(id);
                if (result)
                {
                    TempData["message"] = "Supression du compte effectué avec succès!";
                    List<User> users = UserServices.findAll();
                    if (users.Count != 0)
                    {
                        ViewData["users"] = users;
                    }
                    return RedirectToAction("GestionUsers", "Membre");
                }
                else
                {
                    TempData["error"] = "Erreur dans la supression du compte [" + id + "].";
                    return View();
                }
            }
        }
}


}