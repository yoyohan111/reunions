﻿using MySql.Data.MySqlClient;
using reunions.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace reunions.Providers
{
    public class DAOReunion
    {
        private static DbConnection cnx;
        private static DbCommand cmd;

        public static Reunion findById(int reunion_id)
        {
            Reunion reunion = null;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "SELECT REUNION.ID_REUNION, " +
                        "REUNION.DATE_AJOUT, " +
                        "REUNION.SUJET, " +
                        "REUNION.PLAN, " +
                        "REUNION.DATEE, " +
                        "REUNION.DUREE, " +
                        "REUNION.ADRESSE_LIEU, " +
                        "USERS.*, " +
                        "GROUPE.* " +
                    "FROM REUNION " +
                    "INNER JOIN GROUPE " +
                    "ON GROUPE.ID_GROUPE = REUNION.GROUPE_ID " +
                    "INNER JOIN USERS " +
                    "ON USERS.ID_USER = REUNION.MANAGER_ID" +
                "WHERE REUNION.ID_REUNION = @id;";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "id",
                DbType = System.Data.DbType.Int32,
                Value = reunion_id
            };
            cmd.Parameters.Add(param);
            DbDataReader data = cmd.ExecuteReader();
            if (data.Read())
            {
                User the_manager;
                Groupe the_groupe;
                CompteRendu the_compte_rendu = null;
                the_manager = new User
                {
                    id = Int32.Parse(data["ID_USER"].ToString()),
                    nom = data["NOM"].ToString(),
                    prenom = data["PRENOM"].ToString(),
                    email = data["EMAIL"].ToString(),
                    password = data["PASSWORD"].ToString(),
                    role = Int32.Parse(data["ROLE"].ToString())
                };
                the_groupe = new Groupe
                {
                    id = Int32.Parse(data["ID_GROUPE"].ToString()),
                    date_creation = data["DATE_CREATION"].ToString(),
                    titre = data["TITRE"].ToString(),
                    photo_path = data["PHOTO_PATH"].ToString()
                };
                the_compte_rendu = new CompteRendu
                {
                    id = Int32.Parse(data["ID_COMPTE_RENDU"].ToString()),
                    resume = data["RESUME"].ToString()
                };
                if (the_compte_rendu == null)
                {
                    reunion = new Reunion
                    {
                        id = Int32.Parse(data["ID_USER"].ToString()),
                        manager = the_manager,
                        groupe = the_groupe,
                        date_ajout = data["DATE_AJOUT"].ToString(),
                        sujet = data["SUJET"].ToString(),
                        date = data["DATEE"].ToString(),
                        duree = Int32.Parse(data["DUREE"].ToString()),
                        adresse = data["ADRESSE_LIEU"].ToString()
                    };
                }
                else
                {
                    reunion = new Reunion
                    {
                        id = Int32.Parse(data["ID_USER"].ToString()),
                        manager = the_manager,
                        groupe = the_groupe,
                        compte_rendu = the_compte_rendu,
                        date_ajout = data["DATE_AJOUT"].ToString(),
                        sujet = data["SUJET"].ToString(),
                        date = data["DATEE"].ToString(),
                        duree = Int32.Parse(data["DUREE"].ToString()),
                        adresse = data["ADRESSE_LIEU"].ToString()
                    };
                }
            }
            cnx.Close();
            return reunion;
        }
        public static List<Reunion> findAll()
        {
            List<Reunion> liste = new List<Reunion>();
            Reunion reunion;
            User the_manager;
            Groupe the_groupe;
            CompteRendu the_compte_rendu = null;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "SELECT REUNION.ID_REUNION, " +
                        "REUNION.DATE_AJOUT, " +
                        "REUNION.SUJET, " +
                        "REUNION.PLAN, " +
                        "REUNION.DATEE, " +
                        "REUNION.DUREE, " +
                        "REUNION.ADRESSE_LIEU, " +
                        "USERS.*, " +
                        "GROUPE.* " +
                    "FROM REUNION " +
                    "INNER JOIN GROUPE " +
                    "ON GROUPE.ID_GROUPE = REUNION.GROUPE_ID " +
                    "INNER JOIN USERS " +
                    "ON USERS.ID_USER = REUNION.MANAGER_ID";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbDataReader data = cmd.ExecuteReader();
            while (data.Read())
            {
                the_manager = new User
                {
                    id = Int32.Parse(data["ID_USER"].ToString()),
                    nom = data["NOM"].ToString(),
                    prenom = data["PRENOM"].ToString(),
                    email = data["EMAIL"].ToString(),
                    password = data["PASSWORD"].ToString(),
                    role = Int32.Parse(data["ROLE"].ToString())
                };
                the_groupe = new Groupe
                {
                    id = Int32.Parse(data["ID_USER"].ToString()),
                    date_creation = data["DATE_CREATION"].ToString(),
                    titre = data["TITRE"].ToString(),
                    photo_path = data["PHOTO_PATH"].ToString()
                };
                the_compte_rendu = new CompteRendu
                {
                    id = Int32.Parse(data["ID_USER"].ToString()),
                    resume = data["RESUME"].ToString()
                };
                if (the_compte_rendu == null)
                {
                    reunion = new Reunion
                    {
                        id = Int32.Parse(data["ID_USER"].ToString()),
                        manager = the_manager,
                        groupe = the_groupe,
                        date_ajout = data["DATE_AJOUT"].ToString(),
                        sujet = data["SUJET"].ToString(),
                        plan = data["PLAN"].ToString(),
                        date = data["DATEE"].ToString(),
                        duree = Int32.Parse(data["DUREE"].ToString()),
                        adresse = data["ADRESSE_LIEU"].ToString()
                    };
                } else
                {
                    reunion = new Reunion
                    {
                        id = Int32.Parse(data["ID_USER"].ToString()),
                        manager = the_manager,
                        groupe = the_groupe,
                        compte_rendu = the_compte_rendu,
                        date_ajout = data["DATE_AJOUT"].ToString(),
                        sujet = data["SUJET"].ToString(),
                        plan = data["PLAN"].ToString(),
                        date = data["DATEE"].ToString(),
                        duree = Int32.Parse(data["DUREE"].ToString()),
                        adresse = data["ADRESSE_LIEU"].ToString()
                    };
                }
                liste.Add(reunion);
            }
            cnx.Close();
            return liste;
        }
        public static List<Reunion> findAllForUser(int user_id)
        {
            List<Reunion> liste = new List<Reunion>();
            Reunion reunion;
            User the_manager;
            Groupe the_groupe;
            CompteRendu the_compte_rendu = null;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "SELECT REUNION.ID_REUNION, " +
                        "REUNION.DATE_AJOUT, " +
                        "REUNION.SUJET, " +
                        "REUNION.PLAN, " +
                        "REUNION.DATEE, " +
                        "REUNION.DUREE, " +
                        "REUNION.ADRESSE_LIEU, " +
                        "USERS.*, " +
                        "GROUPE.* " +
                    "FROM REUNION " +
                    "INNER JOIN GROUPE " +
                    "ON GROUPE.ID_GROUPE = REUNION.GROUPE_ID " +
                    "INNER JOIN USERS " +
                    "ON USERS.ID_USER = REUNION.MANAGER_ID WHERE REUNION.GROUPE_ID IN (SELECT groupe_id FROM relation_user_groupe WHERE user_id = @id);";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "id",
                DbType = System.Data.DbType.Int32,
                Value = user_id
            };
            cmd.Parameters.Add(param);
            DbDataReader data = cmd.ExecuteReader();
            while (data.Read())
            {
                the_manager = new User
                {
                    id = Int32.Parse(data["ID_USER"].ToString()),
                    nom = data["NOM"].ToString(),
                    prenom = data["PRENOM"].ToString(),
                    email = data["EMAIL"].ToString(),
                    password = data["PASSWORD"].ToString(),
                    role = Int32.Parse(data["ROLE"].ToString())
                };
                the_groupe = new Groupe
                {
                    id = Int32.Parse(data["ID_USER"].ToString()),
                    date_creation = data["DATE_CREATION"].ToString(),
                    titre = data["TITRE"].ToString(),
                    photo_path = data["PHOTO_PATH"].ToString()
                };
                //the_compte_rendu = new CompteRendu
                //{
                //    id = Int32.Parse(data["ID_USER"].ToString()),
                //    resume = data["RESUME"].ToString()
                //};
                if (the_compte_rendu == null)
                {
                    reunion = new Reunion
                    {
                        id = Int32.Parse(data["ID_USER"].ToString()),
                        manager = the_manager,
                        groupe = the_groupe,
                        date_ajout = data["DATE_AJOUT"].ToString(),
                        sujet = data["SUJET"].ToString(),
                        plan = data["PLAN"].ToString(),
                        date = data["DATEE"].ToString(),
                        duree = Int32.Parse(data["DUREE"].ToString()),
                        adresse = data["ADRESSE_LIEU"].ToString()
                    };
                }
                else
                {
                    reunion = new Reunion
                    {
                        id = Int32.Parse(data["ID_USER"].ToString()),
                        manager = the_manager,
                        groupe = the_groupe,
                        compte_rendu = the_compte_rendu,
                        date_ajout = data["DATE_AJOUT"].ToString(),
                        sujet = data["SUJET"].ToString(),
                        plan = data["PLAN"].ToString(),
                        date = data["DATEE"].ToString(),
                        duree = Int32.Parse(data["DUREE"].ToString()),
                        adresse = data["ADRESSE_LIEU"].ToString()
                    };
                }
                liste.Add(reunion);
            }
            cnx.Close();
            return liste;
        }
        public static bool Creer(Reunion reunion)
        {
            bool resultat;
            cnx = new MySqlConnection();
            cnx.ConnectionString = Config.CONNECTION_STRING;
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "INSERT INTO `reunion`(`MANAGER_ID`, `GROUPE_ID`, `sujet`, `plan`, `datee`, `duree`, `adresse_lieu`) " +
                "VALUES (@id_manager,@id_groupe,@sujet,@plan,@datee,@duree,@adresse_lieu)";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "id_manager",
                DbType = System.Data.DbType.Int32,
                Value = reunion.manager.id
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "id_groupe",
                DbType = System.Data.DbType.Int32,
                Value = reunion.groupe.id
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "sujet",
                DbType = System.Data.DbType.String,
                Value = reunion.sujet
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "plan",
                DbType = System.Data.DbType.String,
                Value = reunion.plan
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "datee",
                DbType = System.Data.DbType.String,
                Value = reunion.date
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "duree",
                DbType = System.Data.DbType.Int32,
                Value = reunion.duree
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "adresse_lieu",
                DbType = System.Data.DbType.String,
                Value = reunion.adresse
            };
            cmd.Parameters.Add(param);

            resultat = cmd.ExecuteNonQuery() > 0;   //bool value because of if > 0
            cnx.Close();
            return resultat;
        }
    }
}