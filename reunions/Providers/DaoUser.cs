﻿using MySql.Data.MySqlClient;
using reunions.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using System.Data;

namespace reunions.Providers
{
    public class DaoUser
    {
        private static DbConnection cnx;
        private static DbCommand cmd;

        internal static User findByEmail(string email)
        {
            User user = null;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM users WHERE `EMAIL` = @Email";
            DbParameter param = cmd.CreateParameter();
            param.ParameterName = "email";
            param.Value = email;
            param.DbType = DbType.String;
            cmd.Parameters.Add(param);
            try
            {
                DbDataReader data = cmd.ExecuteReader();
                if (data.Read())
                {
                    user = new User();
                    user.id = int.Parse(data["ID_USER"].ToString());
                    user.nom = data["NOM"].ToString();
                    user.prenom = data["PRENOM"].ToString();
                    user.email = data["EMAIL"].ToString();
                    user.password = data["PASSWORD"].ToString();
                    user.role = Int32.Parse(data["ROLE"].ToString());
                }
                return user;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return user;
        }

        public static User findById(int id)
        {
            User user = null;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "SELECT * FROM `users` WHERE `ID_USER`= @id";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "id",
                DbType = System.Data.DbType.Int32,
                Value = id
            };
            cmd.Parameters.Add(param);
            DbDataReader data = cmd.ExecuteReader();
            if (data.Read())
            {
                user = new User
                {
                    id = Int32.Parse(data["ID_USER"].ToString()),
                    nom = data["NOM"].ToString(),
                    prenom = data["PRENOM"].ToString(),
                    email = data["EMAIL"].ToString(),
                    password = data["PASSWORD"].ToString(),
                    role = Int32.Parse(data["ROLE"].ToString())
                };
            }
            cnx.Close();
            return user;
        }
        public static List<User> findAll()
        {
            List<User> liste = new List<User>();
            User user;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "SELECT * FROM `users`";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbDataReader data = cmd.ExecuteReader();
            while (data.Read())
            {
                user = new User
                {
                    id = Int32.Parse(data["ID_USER"].ToString()),
                    nom = data["NOM"].ToString(),
                    prenom = data["PRENOM"].ToString(),
                    email = data["EMAIL"].ToString(),
                    password = data["PASSWORD"].ToString(),
                    role = Int32.Parse(data["ROLE"].ToString())
                };
                liste.Add(user);
            }
            cnx.Close();
            return liste;
        }
        public static List<User> findAllByRole(int role)
        {
            List<User> liste = new List<User>();
            User user;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "SELECT * FROM `users` WHERE role = @role";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "role",
                DbType = System.Data.DbType.Int32,
                Value = role
            };
            cmd.Parameters.Add(param);
            DbDataReader data = cmd.ExecuteReader();
            while (data.Read())
            {
                user = new User
                {
                    id = Int32.Parse(data["ID_USER"].ToString()),
                    nom = data["NOM"].ToString(),
                    prenom = data["PRENOM"].ToString(),
                    email = data["EMAIL"].ToString(),
                    password = data["PASSWORD"].ToString(),
                    role = Int32.Parse(data["ROLE"].ToString())
                };
                liste.Add(user);
            }
            cnx.Close();
            return liste;
        }
        public static bool Creer(User user)
        {
            bool resultat;
            cnx = new MySqlConnection();
            cnx.ConnectionString = Config.CONNECTION_STRING;
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "INSERT INTO `users`(`NOM`, `PRENOM`, `EMAIL`, `PASSWORD`, `ROLE`) " +
                "VALUES (@nom,@prenom,@email,@password,@role)";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "nom",
                DbType = System.Data.DbType.String,
                Value = user.nom
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "prenom",
                DbType = System.Data.DbType.String,
                Value = user.prenom
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "email",
                DbType = System.Data.DbType.String,
                Value = user.email
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "password",
                DbType = System.Data.DbType.String,
                Value = user.password
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "role",
                DbType = System.Data.DbType.Int32,
                Value = user.role
            };
            cmd.Parameters.Add(param);

            resultat = cmd.ExecuteNonQuery() > 0;   //bool value because of if > 0
            cnx.Close();
            return resultat;
        }
        public static bool changerRole(int id_user)
        {
            User user = findById(id_user);
            if (user == null)
            {
                return false;
            }
            bool resultat;
            cnx = new MySqlConnection();
            cnx.ConnectionString = Config.CONNECTION_STRING;
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "UPDATE users SET role = @role WHERE ID_USER = @id";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            int newRole;
            if (user.role == 0)
            {
                newRole = 1;
            } else
            {
                newRole = 0;
            }
            DbParameter param = new MySqlParameter
            {
                ParameterName = "role",
                DbType = System.Data.DbType.Int32,
                Value = newRole
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "id",
                DbType = System.Data.DbType.Int32,
                Value = id_user
            };
            cmd.Parameters.Add(param);

            resultat = cmd.ExecuteNonQuery() > 0;   //bool value because of if > 0
            cnx.Close();
            return resultat;
        }
        public static bool delete(int id_user)
        {
            bool resultat;
            cnx = new MySqlConnection();
            cnx.ConnectionString = Config.CONNECTION_STRING;
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "DELETE FROM users WHERE ID_USER = @id";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "id",
                DbType = System.Data.DbType.Int32,
                Value = id_user
            };
            cmd.Parameters.Add(param);

            resultat = cmd.ExecuteNonQuery() > 0;   //bool value because of if > 0
            cnx.Close();
            return resultat;
        }
    }
}