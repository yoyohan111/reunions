﻿using MySql.Data.MySqlClient;
using reunions.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace reunions.Providers
{
    public class DAOGroupe
    {
        private static DbConnection cnx;
        private static DbCommand cmd;
        public static Groupe findById(int id)
        {
            Groupe groupe = null;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "SELECT * FROM `groupe` WHERE `ID_GROUPE`= @id";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "id",
                DbType = System.Data.DbType.Int32,
                Value = id
            };
            cmd.Parameters.Add(param);
            DbDataReader data = cmd.ExecuteReader();
            if (data.Read())
            {
                groupe = new Groupe
                {
                    id = Int32.Parse(data["ID_GROUPE"].ToString()),
                    date_creation = data["DATE_CREATION"].ToString(),
                    titre = data["TITRE"].ToString(),
                    photo_path = data["PHOTO_PATH"].ToString()
                };
            }
            cnx.Close();
            groupe.membres = GetAllMembres(groupe);
            return groupe;
        }
        public static Groupe findByTitre(string titre)
        {
            Groupe groupe = null;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "SELECT * FROM `groupe` WHERE `TITRE`= @t";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "t",
                DbType = System.Data.DbType.String,
                Value = titre
            };
            cmd.Parameters.Add(param);
            DbDataReader data = cmd.ExecuteReader();
            if (data.Read())
            {
                groupe = new Groupe
                {
                    id = Int32.Parse(data["ID_GROUPE"].ToString()),
                    date_creation = data["DATE_CREATION"].ToString(),
                    titre = data["TITRE"].ToString(),
                    photo_path = data["PHOTO_PATH"].ToString()
                };
            }
            cnx.Close();
            groupe.membres = GetAllMembres(groupe);
            return groupe;
        }
        public static List<Groupe> findAll()
        {
            List<Groupe> liste = new List<Groupe>();
            Groupe groupe;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "SELECT * FROM `groupe`";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbDataReader data = cmd.ExecuteReader();
            while (data.Read())
            {
                groupe = new Groupe
                {
                    id = Int32.Parse(data["ID_GROUPE"].ToString()),
                    date_creation = data["DATE_CREATION"].ToString(),
                    titre = data["TITRE"].ToString(),
                    photo_path = data["PHOTO_PATH"].ToString()
                };
                liste.Add(groupe);
            }
            cnx.Close();
            foreach (Groupe gr in liste)
            {
                gr.membres = GetAllMembres(gr);
            }
            return liste;
        }
        public static List<Groupe> findAllForUser(int user_id)
        {
            List<Groupe> liste = new List<Groupe>();
            Groupe groupe;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "SELECT * FROM groupe WHERE ID_GROUPE IN (SELECT DISTINCT groupe_id FROM relation_user_groupe WHERE user_id = @id) ORDER BY date_creation DESC";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "id",
                DbType = System.Data.DbType.Int32,
                Value = user_id
            };
            cmd.Parameters.Add(param);
            DbDataReader data = cmd.ExecuteReader();
            while (data.Read())
            {
                groupe = new Groupe
                {
                    id = Int32.Parse(data["ID_GROUPE"].ToString()),
                    date_creation = data["DATE_CREATION"].ToString(),
                    titre = data["TITRE"].ToString(),
                    photo_path = data["PHOTO_PATH"].ToString()
                };
                liste.Add(groupe);
            }
            cnx.Close();
            foreach (Groupe gr in liste)
            {
                gr.membres = GetAllMembres(gr);
            }
            return liste;
        }
        public static List<User> GetAllMembres(Groupe groupe)
        {
            List<User> liste = new List<User>();
            User user;
            cnx = new MySqlConnection
            {
                ConnectionString = Config.CONNECTION_STRING
            };
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "SELECT * FROM users WHERE ID_USER IN (SELECT DISTINCT user_id FROM relation_user_groupe WHERE groupe_id = @id)";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "id",
                DbType = System.Data.DbType.Int32,
                Value = groupe.id
            };
            cmd.Parameters.Add(param);
            DbDataReader data = cmd.ExecuteReader();
            while (data.Read())
            {
                user = new User
                {
                    id = Int32.Parse(data["ID_USER"].ToString()),
                    nom = data["NOM"].ToString(),
                    prenom = data["PRENOM"].ToString(),
                    email = data["EMAIL"].ToString(),
                    password = data["PASSWORD"].ToString(),
                    role = Int32.Parse(data["ROLE"].ToString())
                };
                liste.Add(user);
            }
            cnx.Close();
            return liste;
        }
        public static bool Creer(Groupe groupe)
        {
            bool resultat;
            cnx = new MySqlConnection();
            cnx.ConnectionString = Config.CONNECTION_STRING;
            cnx.Open();
            cmd = cnx.CreateCommand();
            string requete = "INSERT INTO `groupe`(`TITRE`, `PHOTO_PATH`) " +
                "VALUES (@titre,@photo_path)";
            cmd.CommandText = requete;
            cmd.CommandType = System.Data.CommandType.Text;
            DbParameter param = new MySqlParameter
            {
                ParameterName = "TITRE",
                DbType = System.Data.DbType.String,
                Value = groupe.titre
            };
            cmd.Parameters.Add(param);
            param = new MySqlParameter
            {
                ParameterName = "PHOTO_PATH",
                DbType = System.Data.DbType.String,
                Value = groupe.photo_path
            };
            cmd.Parameters.Add(param);

            resultat = cmd.ExecuteNonQuery() > 0;   //bool value because of if > 0
            cnx.Close();
            Groupe newGroupeWithId = findByTitre(groupe.titre); //ici on est aubligé de le rechercher à cause que le id est créé automatiquement dans la BD...  :-(

            if (resultat)
            {
                foreach (User user in groupe.membres)
                {
                    bool resultat_loop;
                    cnx = new MySqlConnection();
                    cnx.ConnectionString = Config.CONNECTION_STRING;
                    cnx.Open();
                    cmd = cnx.CreateCommand();
                    string requete2 = "INSERT INTO `relation_user_groupe`(`user_id`, `groupe_id`) " +
                        "VALUES (@u_id,@gr_id)";
                    cmd.CommandText = requete2;
                    cmd.CommandType = System.Data.CommandType.Text;
                    param = new MySqlParameter
                    {
                        ParameterName = "u_id",
                        DbType = System.Data.DbType.Int32,
                        Value = user.id
                    };
                    cmd.Parameters.Add(param);
                    param = new MySqlParameter
                    {
                        ParameterName = "gr_id",
                        DbType = System.Data.DbType.Int32,
                        Value = newGroupeWithId.id
                    };
                    cmd.Parameters.Add(param);
                    resultat_loop = cmd.ExecuteNonQuery() > 0;
                    cnx.Close();
                    if (!resultat_loop)
                    {
                        return false;
                    }
                }
                return true;
            } else
            {
                return false;
            }
        }
    }
}