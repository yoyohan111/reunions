﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace reunions.Models
{
    public class UserSignup
    {
        public string prenom { get; set; }
        public string nom { get; set; }
        public string email { get; set; }

        public string password { get; set; }
        public string password2 { get; set; }
    }
}