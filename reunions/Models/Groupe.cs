﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace reunions.Models
{
    public class Groupe
    {
        public int id { get; set; }
        [Required]
        public string titre { get; set; }
        public string photo_path { get; set; }
        public string date_creation { get; set; }
        [Required]
        public List<User> membres { get; set; }
    }
}