﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace reunions.Models
{
    public class Reunion
    {
        public int id { get; set; }
        public User manager { get; set; }
        public Groupe groupe { get; set; }
        public CompteRendu compte_rendu { get; set; }
        public string date_ajout { get; set; }
        [Required]
        public string sujet { get; set; }
        [DataType(DataType.MultilineText)]
        public string plan { get; set; }
        [Required]
        public string date { get; set; }
        [Required]
        public int duree { get; set; }
        [Required]
        public string adresse { get; set; }
    }
}