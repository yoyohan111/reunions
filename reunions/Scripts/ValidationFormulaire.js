/**
 *   Les versions de jQuery plus récents que 3.3.1 BRISE CE SCRIPT... alors ... ouai.. c'est poche.
 */

    
    
jQuery.validator.addMethod(
    "password",
    function(value,element){
        return  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(value) ;
    },
    'Le mot de passe doit contenir : au moins une lettre minuscule,au moins une lettre majuscule, au moins un chiffre,au moins huite caractères ' 
   
    
);

jQuery.validator.addMethod(
    "email",
    function(value,element){
        return  /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@(?:\S{1,63})$/.test( value );
    },
    'Rentrez une adresse email valide ' 
   
    
);

//$.validator.setDefaults( {
//	submitHandler: function () {
  //	alert( "submitted!" );
  //}
//} );


$( document ).ready( function () {
          
           $('input').on('blur', function () {
              if ($("#signupForm").valid()) {
                  $('#submit').prop('disabled', false);
              } else {
                  $('#submit').prop('disabled', 'disabled');
              }
          });
          
  $( "#signupForm" ).validate( {
      rules: {
          prenom: "required",
          email: {
            required: true,
            email: true
          },
          password: {
              required: true,
              minlength: 8
          },
          confirm_password: {
              required: true,
              minlength: 8,
              equalTo: "#password"
          }
      },
      messages: {
          prenom: "Veuillez saisir votre prenom",
          email: { required : "Veuillez saisir un email valide"},
          password: {
              required: "Veuillez saisir votre mot de passe",
              minlength: "Votre mot de passe doit contenir au moins 8 characters"
          },
          confirm_password: {
              required: "Veuillez saisir votre mot de passe",
              minlength: "Votre identifiant doit contenir au moins 8 characters",
              equalTo: "Veuillez saisir le meme mot de passe que précédent"
          }
      },
      errorElement: "em",
      errorPlacement: function ( error, element ) {
          // Add the `invalid-feedback` class to the error element
          error.addClass( "invalid-feedback" );
          
          if ( element.prop( "type" ) === "checkbox" ) {
              error.insertAfter( element.parent( "label" ) );
          } else {
              error.insertAfter( element ); //insérer le message d'erreur!!!
          }

          // Add the em element, if doesn't exists, and apply the icon classes to it.
          if ( !element.next( "em" )[ 0 ] ) {
              $( "<em class='invalid-feedback'></em>" ).insertAfter( element );
          }
      },
      success: function (input, element ) {
          // Add the div feedback element, if doesn't exists, and apply the icon classes to it.
          if ( !$( element ).next( "em" )[ 0 ] ) {
              $( "<em class='invalid-feedback'></em>" ).insertAfter( $( element ) );
          }
      },
      highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            $( element ).next( "em" ).addClass( "invalid-feedback" ).removeClass( "valid-feedback" );
      },
      unhighlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            $( element ).next( "em" ).addClass( "valid-feedback" ).removeClass( "invalid-feedback" );
      }                      
  } );
} );