﻿// script pour afficher le nom du fichier après le téléchargement (upload)
$(document).on('input', '.custom-file-input', function (e) {
    $(this).next('.custom-file-label').text(e.target.files[0].name);
});

//script pour le required de l'Acoredéon
$('span#ErrorMessageAcordeon').hide();
jQuery(function ($) { //pour s'assurer qu'au moins un checkbox est sélectionné pour la liste des élèves
    var requiredCheckboxes = $(':checkbox[required]');
    requiredCheckboxes.on('change', function (e) {
        var checkboxGroup = requiredCheckboxes.filter('[name="' + $(this).attr('name') + '"]');
        var isChecked = checkboxGroup.is(':checked');
        checkboxGroup.prop('required', !isChecked);
    });
    requiredCheckboxes.trigger('change');
});
