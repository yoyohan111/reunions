﻿/* pour la confirmation des actions dans la page GestionUsers */
function supp_user(elem, id_user, prenom) {
    elem.addEventListener('click', function (e) {
        e.preventDefault();
    }, false);
    console.log("Entre dans supp_user avec id_user: " + id_user + "");
    if (id_user != null) {
        if (confirm("Voulez-vous vraiment supprimer le compte de [" + prenom + "] ?")) {
            path = '/Membre/DeleteUser/' + id_user + '';
            window.location.href = path;
            return true;
        }
    }
}
function chg_role_user(elem, id_user, prenom) {
    elem.addEventListener('click', function (e) {
        e.preventDefault();
    }, false);
    console.log("Entre dans chg_role_user avec id_user: " + id_user + "");
    if (id_user != null) {
        if (confirm("Voulez-vous vraiment changer le rôle du compte de [" + prenom + "] ?")) {
            path = '/Membre/ChangeRoleUser/'+id_user + '';
            window.location.href = path;
            return true;
        }
    }
}