﻿using reunions.Models;
using reunions.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace reunions.Services
{
    public class GroupeServices
    {
        public static Groupe findById(int groupe_id)
        {
            return DAOGroupe.findById(groupe_id);
        }
        public static List<Groupe> findAll()
        {
            return DAOGroupe.findAll();
        }
        public static List<Groupe> findAllForUser(int user_id)
        {
            return DAOGroupe.findAllForUser(user_id);
        }
        public static bool Creer(Groupe groupe)
        {
            return DAOGroupe.Creer(groupe);
        }
    }
}