﻿using reunions.Models;
using reunions.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace reunions.Services
{
    public class ReunionServices
    {
        public static Reunion findById(int reunion_id)
        {
            return DAOReunion.findById(reunion_id);
        }
        public static List<Reunion> findAll()
        {
            return DAOReunion.findAll();
        }
        public static List<Reunion> findAllForUser(int user_id)
        {
            return DAOReunion.findAllForUser(user_id);
        }
        public static bool Creer(Reunion reunion)
        {
            return DAOReunion.Creer(reunion);
        }
    }
}