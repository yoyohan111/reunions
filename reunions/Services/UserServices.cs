﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using reunions.Providers;
using reunions.Models;

namespace reunions.Services
{
    public class UserServices
    {
        public static User CheckLogin(string courriel, string pwd)
        {
            User user = DaoUser.findByEmail(courriel);
            if (user == null) return null;
            if (!user.password.Equals(pwd)) return null;
            user.password = "";
            return user;
        }
        public static bool Inscrire(UserSignup userSignup)
        {
            User user = DaoUser.findByEmail(userSignup.email);
            if (user != null)
            {
                return false;
            }
            user = new User
            {
                prenom = userSignup.prenom,
                email = userSignup.email,
                nom = userSignup.nom,
                password = userSignup.password,
                role = 0
            };
            bool result = DaoUser.Creer(user);
            return result;
        }
        public static List<User> findAll()
        {
            return DaoUser.findAll();
        }
        public static List<User> GetAllUtilisateurs()
        {
            return DaoUser.findAllByRole(0);
        }
        public static List<User> GetAllManagers()
        {
            return DaoUser.findAllByRole(1);
        }
        public static bool changerRole(int id_user)
        {
            return DaoUser.changerRole(id_user);
        }
        public static bool delete(int id_user)
        {
            return DaoUser.delete(id_user);
        }
    }
}